[requires]
mesongen/0.1@lostmyflag/testing
glad/0.1.36
glfw/3.3.8
imgui/1.88
nlohmann_json/3.11.2

[options]
glad:spec=gl
glad:gl_profile=core
glad:gl_version=None
glad:gles2_version=3.1
glad:extensions=''

[imports]
./res/bindings, imgui_impl_glfw.cpp -> ./src
./res/bindings, imgui_impl_opengl3.cpp -> ./src
./res/bindings, imgui_impl_glfw.h -> ./include
./res/bindings, imgui_impl_opengl3.h -> ./include
./res/bindings, imgui_impl_opengl3_loader.h -> ./include

[generators]
meson
