#pragma once

#include <nlc/dialect/function_view.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/meta/basics.hpp>
#include "nlc/dialect/assert.hpp"

#include <chrono>
#include <future>

template<typename T> class AsyncRes final {
  public:
    AsyncRes() = default;
    AsyncRes(nlc::function<T()> && function);
    AsyncRes(nlc::function_view<T()> function);
    AsyncRes(std::future<nlc::optional<T>> && future);

    auto is_completed() const -> bool;
    auto wait_for(nlc::time::duration const duration) -> bool;
    auto wait() -> void;

    auto get() const -> T const &;
    auto try_get() const -> T const *;
    auto wait_and_get() -> T const &;

    auto extract() -> T;
    auto try_extract() -> nlc::optional<T>;
    auto wait_and_extract() -> T;

  private:
    // T is wrapped in optional because std::future's
    // template type must be default-constructible
    std::future<nlc::optional<T>> ret_value;
    // std::future drops result ownership when get() is called, so we store it here.
    mutable nlc::optional<T> res;
};

template<typename T>
AsyncRes<T>::AsyncRes(nlc::function_view<T()> function)
    : AsyncRes<T> { std::async(std::launch::async,
                               [function]() -> nlc::optional<T> { return function(); }) } {}

template<typename T>
AsyncRes<T>::AsyncRes(nlc::function<T()> && function)
    : AsyncRes<T> { std::async(std::launch::async,
                               [f = nlc::move(function)]() -> nlc::optional<T> { return f(); }) } {}

template<typename T>
AsyncRes<T>::AsyncRes(std::future<nlc::optional<T>> && future)
    : ret_value { nlc::move(future) } {}

template<typename T> auto AsyncRes<T>::wait_for(nlc::time::duration const duration) -> bool {
    if (res != nlc::null)
        return true;

    auto const status { ret_value.wait_for(
        std::chrono::milliseconds { nlc::time::milliseconds<unsigned long>(duration) }) };
    return status == std::future_status::ready;
}

template<typename T> auto AsyncRes<T>::wait() -> void {
    if (res != nlc::null)
        ret_value.wait();
}

template<typename T> auto AsyncRes<T>::is_completed() const -> bool {
    if (res != nlc::null)
        return true;

    auto const status { ret_value.wait_for(std::chrono::milliseconds { 0 }) };
    return status == std::future_status::ready;
}

template<typename T> auto AsyncRes<T>::get() const -> T const & {
    nlc_assert(is_completed());
    nlc_assert_msg(ret_value.valid() == (res == nlc::null),
                   nlc_dump_var(ret_value.valid()),
                   nlc_dump_var(res == nlc::null));
    // get() is not const because it calls wait()
    if (res == nlc::null)
        res = const_cast<AsyncRes<T> *>(this)->ret_value.get().extract();
    return *res;
}

template<typename T> auto AsyncRes<T>::try_get() const -> T const * {
    if (is_completed() == false)
        return nlc::null;
    return &get();
}

template<typename T> auto AsyncRes<T>::wait_and_get() -> T const & {
    wait();
    return get();
}

template<typename T> auto AsyncRes<T>::extract() -> T {
    nlc_assert(is_completed());
    nlc_assert_msg(ret_value.valid() == (res == nlc::null),
                   nlc_dump_var(ret_value.valid()),
                   nlc_dump_var(res == nlc::null));
    // get() is not const because it calls wait()
    if (res == nlc::null)
        res = const_cast<AsyncRes<T> *>(this)->ret_value.get().extract();
    return res.extract();
}

template<typename T> auto AsyncRes<T>::try_extract() -> nlc::optional<T> {
    if (is_completed())
        return nlc::null;
    return extract();
}

template<typename T> auto AsyncRes<T>::wait_and_extract() -> T {
    wait();
    return extract();
}
