#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/meta/id.hpp>
#include <nlc/meta/numeric_limits.hpp>

namespace impl {
struct PorjectIdDesc final {
    using underlying_type = u64;
    static constexpr underlying_type invalid_value = nlc::meta::limits<underlying_type>::max;
    static constexpr underlying_type default_value = invalid_value;
};
struct JobIdDesc final {
    using underlying_type = u64;
    static constexpr underlying_type invalid_value = nlc::meta::limits<underlying_type>::max;
    static constexpr underlying_type default_value = invalid_value;
};
}  // namespace impl

using ProjectId = nlc::meta::id<impl::PorjectIdDesc>;
using JobId = nlc::meta::id<impl::JobIdDesc>;
