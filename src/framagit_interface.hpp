#pragma once

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>

#include "async_res.hpp"
#include "ids.hpp"
#include "settings.hpp"

namespace nlc {
class allocator;
}

struct CommandExecutionFailed final {};
struct JsonIsErrorMessage final {};
struct UnexpectedJsonFormat final {};

auto check_is_project_valid(nlc::allocator & allocator,
                            nlc::string_view const framagit_instance_url,
                            ProjectId const project_id) -> AsyncRes<bool>;

using GetSucceededJobsRes =
    nlc::expected<nlc::vector<JobId>, CommandExecutionFailed, JsonIsErrorMessage, UnexpectedJsonFormat>;
auto get_succeeded_jobs(nlc::allocator & allocator,
                        nlc::string_view const framagit_instance_url,
                        nlc::string_view const private_token,
                        ProjectId const project_id,
                        nlc::span<nlc::string const> tag_filters) -> AsyncRes<GetSucceededJobsRes>;

auto dowload_artifacts(nlc::string_view const framagit_instance_url,
                       nlc::string_view const private_token,
                       ProjectId const project_id,
                       JobId const job_id,
                       nlc::string_view const output_path) -> bool;
