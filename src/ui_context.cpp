/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#include "ui_context.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/fundamentals/from_string.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/meta/units.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include "async_res.hpp"
#include "framagit_interface.hpp"
#include "ids.hpp"
#include "job_list_logic.hpp"
#include "project_logic.hpp"
#include "settings.hpp"
#include "ui_fields.hpp"

constexpr static auto footer_note_timeout { 3_s };

static ImVec4 const white { 1.0f, 1.0f, 1.0f, 1.0f };
static ImVec4 const red { 0.8f, 0.0f, 0.0f, 1.0f };

static auto draw_jobs(nlc::allocator & allocator,
                      Settings const & settings,
                      JobListLogic const & logic,
                      float const footer_size) -> nlc::optional<FooterNote> {
    ImGui::TextWrapped("Found " priusize " jobs !", logic.get_jobs().size());

    ImGui::BeginGroup();
    defer { ImGui::EndGroup(); };

    constexpr ImGuiWindowFlags child_flags { 0 };
    constexpr bool has_border { false };
    const bool is_child_visible { ImGui::BeginChild("##jobs",
                                                    ImVec2(ImGui::GetContentRegionAvail().x,
                                                           ImGui::GetContentRegionAvail().y - footer_size +
                                                               ImGui::GetStyle().ItemSpacing.y),
                                                    has_border,
                                                    child_flags) };
    defer { ImGui::EndChild(); };

    if (is_child_visible == false)
        return nlc::null;

    auto const id_to_doawnload_artifacts { logic.draw_ui() };
    if (id_to_doawnload_artifacts.is_valid()) {
        constexpr nlc::string_view artifact_path { "artifacts.zip" };
        auto const success { dowload_artifacts(settings.framagit_instance_url,
                                               settings.private_token,
                                               settings.project_id,
                                               id_to_doawnload_artifacts,
                                               artifact_path) };

        if (success)
            return FooterNote { allocator, "Successfully downloaded artifacts !", FooterNote::Type::Info };
        else
            return FooterNote { allocator, "Couldn't download artifacts", FooterNote::Type::Error };
    }

    return nlc::null;
}

static auto draw_footer(FooterNote const & note, float const footer_size) -> void {
    auto const color = [&note] {
        switch (note.type) {
            case FooterNote::Type::Info: return white;
            case FooterNote::Type::Error: return red;
        }

        unreachable;
    }();

    ImGui::SetCursorPosY(ImGui::GetIO().DisplaySize.y - footer_size);
    ImGui::Separator();
    ImGui::TextColored(color, "%s", note.message.c_str());
}

UIContext::UIContext(GLFWwindow & window)
    : footer_msg {
        internal_allocator,
        "",
        FooterNote::Type::Info,
    } {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(&window, true);
    ImGui_ImplOpenGL3_Init("#version 300 es");

    // Init for first frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

UIContext::~UIContext() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

auto UIContext::update(nlc::allocator & settings_allocator, Settings & settings) -> void {
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    defer { ImGui::PopStyleVar(); };

    static nlc::standard_allocator local_allocator;
    static EditableSettings edited_settings { local_allocator, settings };
    static ProjectLogic project_logic;
    static JobListLogic job_list_logic;

    {
        auto const footer_size { ImGui::GetTextLineHeightWithSpacing() +
                                 ImGui::GetStyle().ItemSpacing.y };

        ImGui::Begin("main window", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoResize);
        defer { ImGui::End(); };

        bool const have_project_settings_changed { edited_settings.draw_ui() };
        project_logic.update(local_allocator, edited_settings, have_project_settings_changed);

        bool const are_project_settings_valid { project_logic.get_state() ==
                                                ProjectLogic::State::valid_project };
        if (are_project_settings_valid)
            settings = edited_settings.get_settings(settings_allocator);

        job_list_logic.update(local_allocator, settings, are_project_settings_valid);

        switch (project_logic.get_state()) {
            case ProjectLogic::State::curl_unavailable:
                ImGui::Text("/!\\ curl is missing /!\\");
                break;
            case ProjectLogic::State::checking_project_validity:
                ImGui::Text("Checking project validity...");
                break;
            case ProjectLogic::State::valid_project: ImGui::Text("The project is valid !"); break;
            case ProjectLogic::State::invalid_project:
                ImGui::TextWrapped("The project seems to be invalid. Please check the Framagit "
                                   "instance URL and the project ID");
                break;
        }

        if (ImGui::Button("Save settings")) {
            settings = edited_settings.get_settings(settings_allocator);
            edited_settings.set_settings(local_allocator, settings);
            save_settings(settings);
        }

        ImGui::SameLine();

        if (ImGui::Button("Reset settings")) {
            nlc::string_stream footer_str { local_allocator };
            auto loaded { load_settings(settings_allocator, footer_str) };
            if (loaded == nlc::null)
                set_footer_message(footer_str, FooterNote::Type::Error);
            else {
                settings = loaded.extract();
                edited_settings.set_settings(local_allocator, settings);
                set_footer_message("Successfully loaded settings", FooterNote::Type::Info);
            }
        }

        switch (job_list_logic.get_state()) {
            case JobListLogic::State::invalid_settings: break;
            case JobListLogic::State::fetching_jobs:
                ImGui::TextWrapped("Fetching succeeded jobs...");
                break;
            case JobListLogic::State::ready: {
                if (auto const msg = job_list_logic.get_error_msg(); msg != nlc::null) {
                    set_footer_message(*msg, FooterNote::Type::Error);
                    break;
                }

                auto msg { draw_jobs(internal_allocator, settings, job_list_logic, footer_size) };
                if (msg != nlc::null)
                    set_footer_message(msg.extract());
                break;
            }
        }

        if (nlc::time::now() >= footer_msg_hide_time) {
            footer_msg.message = { internal_allocator, nullptr, 0u };
            footer_msg_hide_time = never;
        }

        draw_footer(footer_msg, footer_size);
    }
}

auto UIContext::set_footer_message(FooterNote && note) -> void {
    footer_msg = nlc::move(note);
    footer_msg_hide_time = nlc::time::now() + footer_note_timeout;
}

auto UIContext::set_footer_message(nlc::string_view const msg, FooterNote::Type const type) -> void {
    footer_msg = { internal_allocator, msg, type };
    footer_msg_hide_time = nlc::time::now() + footer_note_timeout;
}
