#include "settings.hpp"

#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/read_write_file.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/units.hpp>
#include <nlc/os/file_system.hpp>

#include <mdf/compiler.hpp>
#include <mdf/dump.hpp>
#include <mdf/types.hpp>

constexpr nlc::string_view settings_file { "settings.mdf" };
constexpr nlc::string_view instance_url_key { "FramagitInstanceURL" };
constexpr nlc::string_view project_id_key { "ProjectId" };
constexpr nlc::string_view private_token_key { "PrivateToken" };
constexpr nlc::string_view job_tags_key { "JobTagFilters" };

Settings::Settings(nlc::allocator & allocator)
    : framagit_instance_url { allocator, "" }
    , project_id { 0u }
    , private_token { allocator, "" }
    , job_tag_filters { allocator } {}

auto Settings::hash() const -> nlc::hash_result {
    return nlc::hash_result { nlc::hash(framagit_instance_url).value() ^
                              nlc::hash(project_id).value() ^ nlc::hash(private_token).value() };
}

auto load_settings(nlc::allocator & allocator) -> nlc::optional<Settings> {
    nlc::string_stream error_msg { allocator };
    return load_settings(allocator, error_msg);
}

auto load_settings(nlc::allocator & allocator, nlc::string_stream & error_msg)
    -> nlc::optional<Settings> {
    constexpr nlc::string_view footer_msg { "Couldn't load settings from disk. See console for "
                                            "more info" };

    auto const file_exists_result { nlc::does_file_exist(settings_file) };
    if (file_exists_result.failed() || file_exists_result.get() == false) {
        error_msg << "file '" << settings_file << "' does not exist";
#ifndef NDEBUG
        if (file_exists_result.failed())
            error_msg << ": " << file_exists_result.get_error_name();
#endif
        return nlc::null;
    }

    auto const inputs_and_result { mdf::read_and_compile(allocator, settings_file) };
    auto const & errors { inputs_and_result.compilation_result.ctx().errors() };

    if (errors.is_empty() == false) {
        error_msg << footer_msg;

        nlc::log_entry log;
        mdf::log_errors(nlc::make_span(errors), log);
        nlc::warn("load_settings: MDF compilation errors:\n", log.c_str());

        return nlc::null;
    }

    auto const & root { inputs_and_result.compilation_result.root() };

    bool success { true };
    nlc::string_stream error_str { allocator };

    auto const instance_url { root.try_get<nlc::string_view>(instance_url_key) };
    if (instance_url.failed()) {
        success = false;
        error_str << "Couldn't find '" << instance_url_key << "' string\n";
    }

    auto const project_id { root.try_get<ProjectId::underlying_type>(project_id_key) };
    if (project_id.failed()) {
        success = false;
        error_str << "Couldn't find '" << project_id_key << "' unsigned integer\n";
    }

    auto const private_token { root.try_get<nlc::string_view>(private_token_key) };
    if (private_token.failed()) {
        success = false;
        error_str << "Couldn't find '" << private_token_key << "' string\n";
    }

    auto const job_tag_defs { root.try_get<mdf::Array>(job_tags_key) };
    if (job_tag_defs.failed()) {
        success = false;
        error_str << "Couldn't find '" << job_tags_key << "' string\n";
    }

    if (success == false) {
        error_msg << footer_msg;
        nlc::warn(error_str.c_str());
        return nlc::null;
    }

    auto const job_tag_elems { job_tag_defs.get()->elements<nlc::string_view>() };

    Settings res { allocator };

    res.framagit_instance_url = { allocator, instance_url.get() };
    res.project_id = ProjectId { project_id.get() };
    res.private_token = { allocator, private_token.get() };
    res.job_tag_filters.reserve(job_tag_elems.size());
    for (auto const & tag : job_tag_elems)
        res.job_tag_filters.append_no_grow(allocator, tag);

    return res;
}

auto save_settings(Settings const & settings) -> void {
    nlc::standard_allocator allocator;
    mdf::Object root { allocator };

    root._fields.emplace(instance_url_key, nlc::string_view { settings.framagit_instance_url });
    root._fields.emplace(project_id_key, static_cast<i64>(settings.project_id.value()));
    root._fields.emplace(private_token_key, nlc::string_view { settings.private_token });

    auto const tag_count { settings.job_tag_filters.size() };
    auto tag_elements { nlc::vector<mdf::Rhs>::create_and_reserve(allocator, tag_count) };
    auto source_locs { nlc::vector<mdf::SourceLocation>::create_and_reserve(allocator, tag_count) };
    for (auto i = 0u; i < tag_count; ++i) {
        tag_elements.append_no_grow(nlc::string_view { settings.job_tag_filters[i] });
        source_locs.append_no_grow(
            mdf::SourceLocation { ._line = 0, ._col = 0, ._len = 0, ._fileId = 0 });
    }

    mdf::Array tags { nlc::move(tag_elements), nlc::move(source_locs) };
    root._fields.emplace(job_tags_key, &tags);

    mdf::DumpStyleConfig const dump_config { .key_sort_func = nlc::null };
    nlc::string_stream output_str { allocator };
    mdf::dump(allocator, output_str, root, dump_config, true);

    nlc::write_file(settings_file.ptr(),
                    { reinterpret_cast<byte const *>(output_str.c_str()),
                      nlc::rm_unit(output_str.size()) });
}
