#pragma once

#include <nlc/dialect/optional.hpp>

#include "async_res.hpp"
#include "ids.hpp"
#include "ui_fields.hpp"

struct Settings;
namespace nlc {
class allocator;
}

struct EditableSettings final {
    EditableString framagit_instance_url;
    EditableIntAsString<ProjectId::underlying_type> project_id;
    EditableString private_token;

    EditableSettings(nlc::allocator & allocator, Settings const & settings);

    auto get_project_id() const -> ProjectId;
    auto get_settings(nlc::allocator & allocator) const -> Settings;
    auto set_settings(nlc::allocator & allocator, Settings & settings) -> void;
    auto draw_ui() -> bool;
};

class ProjectLogic final {
  public:
    enum class State {
        curl_unavailable,
        checking_project_validity,
        valid_project,
        invalid_project
    };

    auto update(nlc::allocator & allocator,
                EditableSettings const & settings,
                bool const have_settings_changed) -> void;
    auto get_state() -> State { return state; }

  private:
    nlc::optional<AsyncRes<bool>> is_project_valid_result;
    State state { has_curl ? State::checking_project_validity : State::curl_unavailable };
    static bool const has_curl;
    bool needs_to_check_project_validity { true };
};
