#pragma once

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/fundamentals/hash.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_stream.hpp>

#include "ids.hpp"

namespace nlc {
class allocator;
}

struct Settings final {
    nlc::string framagit_instance_url;
    ProjectId project_id;
    nlc::string private_token;
    nlc::vector<nlc::string> job_tag_filters;

    Settings(nlc::allocator & allocator);

    auto hash() const -> nlc::hash_result;
};

auto load_settings(nlc::allocator & allocator) -> nlc::optional<Settings>;
auto load_settings(nlc::allocator & allocator, nlc::string_stream & error_msg)
    -> nlc::optional<Settings>;
auto save_settings(Settings const & settings) -> void;
