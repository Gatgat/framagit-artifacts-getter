#pragma once

#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/from_string.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/fundamentals/to_string.hpp>

#include <imgui.h>

namespace nlc {
class allocator;
}

struct EditableString {
    nlc::string initial_value;
    nlc::string_stream value;
    nlc::string modified_label;
    nlc::string unmodified_label;
    ImGuiInputTextFlags default_flags;

    EditableString(nlc::allocator & allocator,
                   nlc::string_view const init_val,
                   nlc::string_view const label,
                   ImGuiInputTextFlags const flags = ImGuiInputTextFlags_CallbackResize);

    auto get_label() const -> nlc::string_view;
    auto reset(nlc::allocator & allocator, nlc::string_view const init_val) -> void;
    auto draw_ui(ImGuiInputTextFlags const flags = 0) -> bool;
};

template<typename T> struct EditableIntAsString final : public EditableString {
    EditableIntAsString(nlc::allocator & allocator,
                        T const init_val,
                        nlc::string_view const label,
                        T const def_value = 0u)
        : EditableString { allocator,
                           nlc::to_string(allocator, init_val),
                           label,
                           ImGuiInputTextFlags_CallbackResize | ImGuiInputTextFlags_CallbackCharFilter }
        , default_value { def_value } {}

    auto get_value() const -> T {
        auto const res { nlc::from_string<T>(value) };
        return res != nlc::null ? *res : default_value;
    }

    T const default_value;
};
