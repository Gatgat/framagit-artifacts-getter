/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#include "window_context.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>

#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

static auto on_glfw_error(int const code, char const * const message) -> void {
    nlc::warn(false, "GLFW error ", code, ": ", message);
}

[[nodiscard]] static auto make_null_window() -> nlc::scoped<GLFWwindow *> {
    return { nullptr, [](auto * const) {} };
}

[[nodiscard]] static auto create_window(u32 const width, u32 const height, char const * const title)
    -> nlc::scoped<GLFWwindow *> {
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    GLFWwindow * window =
        glfwCreateWindow(static_cast<int>(width), static_cast<int>(height), title, nullptr, nullptr);

    if (window == nullptr) {
        nlc::warn("couldn't create an OpenGl ES window, falling back on OpenGl 4.5");
        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
        window =
            glfwCreateWindow(static_cast<int>(width), static_cast<int>(height), title, nullptr, nullptr);
        if (window == nullptr) {
            nlc::warn("couldn't create a window of size ", width, "x", height);
            return make_null_window();
        }
    }

    glfwMakeContextCurrent(window);

    if (gladLoadGLES2Loader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)) == 0)
        return make_null_window();

    return { window, [](auto * const ptr) { glfwDestroyWindow(ptr); } };
}

WindowContext::WindowContext(u32 const width, u32 const height, char const * const title)
    : window { make_null_window() } {
    glfwSetErrorCallback(on_glfw_error);

    [[maybe_unused]] auto const glfw_res { glfwInit() };
    nlc_assert_msg(glfw_res, "glfwInit failed");

    window = create_window(width, height, title);
    nlc_assert(*window != nullptr);
}

WindowContext::~WindowContext() {
    window = make_null_window();
    glfwTerminate();
}

auto WindowContext::should_close() const -> bool { return glfwWindowShouldClose(*window); }

auto WindowContext::begin_frame() -> void {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwPollEvents();

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

auto WindowContext::end_frame() -> void { glfwSwapBuffers(*window); }
