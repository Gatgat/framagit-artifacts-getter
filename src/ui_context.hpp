/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#pragma once

#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/meta/numeric_limits.hpp>

struct GLFWwindow;
struct Settings;
namespace nlc {
class allocator;
}

struct FooterNote final {
    enum class Type { Info, Error };

    nlc::string message;
    Type type;

    FooterNote(nlc::allocator & allocator, nlc::string_view const msg, Type const t)
        : message { allocator, msg }
        , type { t } {}
};

class UIContext final {
  public:
    UIContext(GLFWwindow & window);
    ~UIContext();

    auto update(nlc::allocator & allocator, Settings & settings) -> void;

  private:
    auto set_footer_message(FooterNote && note) -> void;
    auto set_footer_message(nlc::string_view const msg, FooterNote::Type const type) -> void;

  private:
    static constexpr auto never { nlc::meta::limits<nlc::time::duration::underlying>::max };
    nlc::standard_allocator internal_allocator;
    FooterNote footer_msg;
    nlc::time::duration footer_msg_hide_time { never };
};
