#include "job_list_logic.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/null.hpp>

#include <imgui.h>

auto JobListLogic::update(nlc::allocator & allocator,
                          Settings const & settings,
                          bool const are_settings_valid) -> void {
    auto const can_fetch_jobs { fetch_jobs_result == nlc::null || fetch_jobs_result->is_completed() };

    if (are_settings_valid == false) {
        state = State::invalid_settings;
        fetch_jobs_result = nlc::null;
        return;
    }

    auto const current_settings_hash { nlc::hash(settings) };
    if (can_fetch_jobs && current_settings_hash != settings_hash) {
        fetch_jobs_result = get_succeeded_jobs(allocator,
                                               settings.framagit_instance_url,
                                               settings.private_token,
                                               settings.project_id,
                                               nlc::make_span(settings.job_tag_filters));
        error_message = nlc::null;
        settings_hash = current_settings_hash;
    }

    nlc_assert(fetch_jobs_result != nlc::null);
    state = fetch_jobs_result->is_completed() ? State::ready : State::fetching_jobs;
    if (state != State::ready)
        return;

    if (error_message == nlc::null) {
        auto const & res { fetch_jobs_result->get() };
        if (res.failed_with<CommandExecutionFailed>())
            error_message = "curl command failed";
        else if (res.failed_with<JsonIsErrorMessage>())
            error_message = "the request failed";
        else if (res.failed_with<UnexpectedJsonFormat>())
            error_message = "couldn't fetch info from the request result";
    }
}

auto JobListLogic::get_jobs() const -> nlc::span<JobId const> {
    if (fetch_jobs_result == nlc::null || fetch_jobs_result->is_completed() == false ||
        fetch_jobs_result->get().failed()) {
        return {};
    }
    return nlc::make_span(fetch_jobs_result->get().get());
}

auto JobListLogic::draw_ui() const -> JobId {
    auto download_artifacts_id { JobId::invalid() };

    for (auto const & job_id : get_jobs()) {
        ImGui::Text(priu64, job_id.value());
        ImGui::SameLine();

        ImGui::PushID(static_cast<int>(job_id.value()));
        defer { ImGui::PopID(); };

        if (ImGui::Button("Download artifacts"))
            download_artifacts_id = job_id;
    }

    return download_artifacts_id;
}
