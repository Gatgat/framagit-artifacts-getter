#include "ui_fields.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/meta/units.hpp>

#include <imgui.h>

static auto make_editable_string_label(nlc::allocator & allocator,
                                       nlc::string_view const displayed_label,
                                       bool const is_modified) -> nlc::string {
    // We want to add a '*' at the end of the label if it was changed.
    // Unfortunately, it changes the hash of the labal which is used to give id,
    // so doing this will lose the focus. To prevent this behaviour, it's possible to append '###ID'
    // at the end of the label.
    nlc::string_stream str { allocator };

    str << displayed_label;
    if (is_modified)
        str << '*';
    str << "###" << displayed_label;

    return { allocator, str };
}

static auto resize_stream(ImGuiInputTextCallbackData & data) -> int {
    nlc_assert(data.BufSize > 0);
    nlc_assert(data.BufTextLen >= 0);

    auto & stream = *reinterpret_cast<nlc::string_stream *>(data.UserData);
    nlc_assert(stream.c_str() == data.Buf);

    auto const txt_size = static_cast<usize>(data.BufTextLen);
    stream.resize(txt_size);
    nlc_assert(nlc::rm_unit(stream.size()) == txt_size);

    data.Buf = stream.c_str();

    return 0;
}

static auto filter_char(ImGuiInputTextCallbackData & data) -> int {
    if (data.EventChar < '0' || data.EventChar > '9')
        return 1;
    return 0;
}

static auto text_input_callback(ImGuiInputTextCallbackData * const data) -> int {
    nlc_assert(data != nullptr);

    switch (data->EventFlag) {
        case ImGuiInputTextFlags_CallbackResize: return resize_stream(*data);
        case ImGuiInputTextFlags_CallbackCharFilter: return filter_char(*data);
        default:
            nlc_assert_msg(false, "text_input_callback: unhandled event flag ", data->EventFlag);
            break;
    }

    return 0;
}

EditableString::EditableString(nlc::allocator & allocator,
                               nlc::string_view const init_val,
                               nlc::string_view const label,
                               ImGuiInputTextFlags const flags)
    : initial_value { allocator, init_val }
    , value { allocator }
    , modified_label { make_editable_string_label(allocator, label, true) }
    , unmodified_label { make_editable_string_label(allocator, label, false) }
    , default_flags { flags } {
    value << initial_value;
}

auto EditableString::get_label() const -> nlc::string_view {
    bool const is_modified { nlc::string_view { initial_value } != nlc::string_view { value } };
    return is_modified ? modified_label : unmodified_label;
}

auto EditableString::reset(nlc::allocator & allocator, nlc::string_view const init_val) -> void {
    initial_value = { allocator, init_val };
    value.clear();
    value << init_val;
}

auto EditableString::draw_ui(ImGuiInputTextFlags const flags) -> bool {
    nlc_assert((flags & ImGuiInputTextFlags_CallbackResize) == 0);
    return ImGui::InputText(get_label().ptr(),
                            value.c_str(),
                            nlc::rm_unit(value.size()) + 1,
                            default_flags | flags,
                            text_input_callback,
                            reinterpret_cast<void *>(&value));
}
