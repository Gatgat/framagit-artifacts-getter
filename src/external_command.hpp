#pragma once

#include <nlc/dialect/string_view.hpp>

#include "async_res.hpp"

namespace nlc {
class allocator;
}

auto execute_command_sync(nlc::string_view const command) -> int;
auto execute_command_async(nlc::allocator & allocator, nlc::string_view const command)
    -> AsyncRes<int>;
