#include "project_logic.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/fundamentals/from_string.hpp>
#include <nlc/fundamentals/to_string.hpp>

#include "external_command.hpp"
#include "framagit_interface.hpp"
#include "settings.hpp"

#include <imgui.h>

bool const ProjectLogic::has_curl { execute_command_sync("curl --help") == 0 };

EditableSettings::EditableSettings(nlc::allocator & allocator, Settings const & settings)
    : framagit_instance_url { allocator, settings.framagit_instance_url, "Framagit instance URL" }
    , project_id { allocator, settings.project_id.value(), "Project ID" }
    , private_token { allocator, settings.private_token, "Private token" } {}

auto EditableSettings::get_project_id() const -> ProjectId {
    return ProjectId { project_id.get_value() };
}

auto EditableSettings::get_settings(nlc::allocator & allocator) const -> Settings {
    Settings res { allocator };

    res.framagit_instance_url = { allocator, framagit_instance_url.value };
    res.project_id = get_project_id();
    res.private_token = { allocator, private_token.value };

    return res;
}

auto EditableSettings::set_settings(nlc::allocator & allocator, Settings & settings) -> void {
    framagit_instance_url.reset(allocator, settings.framagit_instance_url);
    project_id.reset(allocator, nlc::to_string(allocator, settings.project_id.value()));
    private_token.reset(allocator, settings.private_token);
}

auto EditableSettings::draw_ui() -> bool {
    bool changed { false };

    changed |= framagit_instance_url.draw_ui();
    changed |= project_id.draw_ui();
    changed |= private_token.draw_ui();

    return changed;
}

auto ProjectLogic::update(nlc::allocator & allocator,
                          EditableSettings const & settings,
                          bool const have_settings_changed) -> void {
    if (has_curl == false)
        return;

    needs_to_check_project_validity |= have_settings_changed;
    auto const can_check_project_vality { is_project_valid_result == nlc::null ||
                                          is_project_valid_result->is_completed() };

    if (needs_to_check_project_validity && can_check_project_vality) {
        is_project_valid_result = check_is_project_valid(allocator,
                                                         settings.framagit_instance_url.value,
                                                         settings.get_project_id());
        needs_to_check_project_validity = false;
    }

    nlc_assert(is_project_valid_result != nlc::null);
    if (is_project_valid_result->is_completed() == false)
        state = State::checking_project_validity;
    else if (is_project_valid_result->get())
        state = State::valid_project;
    else
        state = State::invalid_project;
}
