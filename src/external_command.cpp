#include "external_command.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/fundamentals/function.hpp>

#include <stdlib.h>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#    include <Windows.h>
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
static auto execute_command_sync_impl(char * const command) -> int {
    PROCESS_INFORMATION process_info;
    STARTUPINFO startup_info;

    ZeroMemory(&process_info, sizeof(process_info));
    ZeroMemory(&startup_info, sizeof(startup_info));
    startup_info.cb = sizeof(startup_info);

    if (CreateProcess(nullptr,
                      command,
                      nullptr,
                      nullptr,
                      FALSE,
                      CREATE_NO_WINDOW,
                      nullptr,
                      nullptr,
                      &startup_info,
                      &process_info) == 0) {
        return 1;
    }

    defer {
        CloseHandle(process_info.hProcess);
        CloseHandle(process_info.hThread);
    };

    WaitForSingleObject(process_info.hProcess, INFINITE);
    DWORD exit_code;
    if (GetExitCodeProcess(process_info.hProcess, &exit_code) == FALSE) {
        nlc::warn("GetExitCodeProcess failed");
        return 1;
    }

    return static_cast<int>(exit_code);
}
#else
static auto execute_command_sync_impl(char const * const command) -> int { return system(command); }
#endif

auto execute_command_sync(nlc::string_view const command) -> int {
    char buffer[1024];
    nlc::make_null_terminated(command, buffer);
    nlc::print("execute_command_sync: executing '", command, "'");
    return execute_command_sync_impl(buffer);
}

auto execute_command_async(nlc::allocator & allocator, nlc::string_view const command)
    -> AsyncRes<int> {
    nlc::print("execute_command_async: executing '", command, "'");
    return { nlc::function<int()> { allocator, [command] { return execute_command_sync(command); } } };
}
