# TODO

## Settings
- hint when hovering fields
- add artifact output path field

## Commands
- integrate in `nlc`
- improve feedback when `curl` is not available
- no console in Windows

## Jobs
- improve jobs informations (name, commit, date, branch)
- allow filtering (by name, tags, branch ?)
- handle archive cache
- allow archive unzipping if `unzip` or `7z` is available
- add "open" button to show downloaded / unzipped artifacts
