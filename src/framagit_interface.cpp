#include "framagit_interface.hpp"

#include <string>
#include <nlohmann/json.hpp>
#include <stdio.h>
#include <stdlib.h>

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/fundamentals/read_write_file.hpp>
#include <nlc/fundamentals/scoped.hpp>
#include <nlc/fundamentals/string_stream.hpp>

#include "external_command.hpp"

static auto stdstr2view(std::string const & str) -> nlc::string_view {
    return { str.c_str(), str.size() };
}

static auto get_json_from_file(nlc::allocator & allocator, nlc::string_view const filename)
    -> nlc::optional<nlohmann::json> {
    auto const output_content { nlc::read_file(allocator, filename) };

    // An exception is thrown if the file is not json.
    // It can happen if curl returns html.
    try {
        return nlohmann::json::parse(output_content->begin(), output_content->end());
    } catch (...) { return nlc::null; }
}

static auto is_json_error(nlohmann::json const & json) -> bool {
    constexpr nlc::string_view error_msg_key { "message" };
    if (json.is_object() == false || json.contains(error_msg_key.ptr()) == false)
        return false;

    nlc::warn("JSON contains an error message: '",
              stdstr2view(json[error_msg_key.ptr()].get<std::string>()),
              "'");
    return true;
}

static auto remove_file(nlc::string_view const cmd) -> bool {
    char buffer[512];
    nlc::make_null_terminated(cmd, buffer);
    return remove(buffer) == 0;
}

static auto do_tags_match_filters(nlohmann::json const & /*tags*/,
                                  nlc::span<nlc::string const> /*filters*/)
    -> nlc::expected<bool, UnexpectedJsonFormat> {
    /*
if (tags.is_array() == false)
    return UnexpectedJsonFormat {};

for (auto const & tag : tags) {
    if (tag.is_string() == false)
        return UnexpectedJsonFormat {};

    auto const tag_str { tag.get<std::string>() };
    auto const tag_view { stdstr2view(tag_str) };

    for (auto const & filter : filters) {
        if (tag_view == filter)
            return true;
    }
}

return false;
/*/
    // TODO: reactivate filtering
    return true;
    //*/
}

static auto check_is_project_valid_sync(nlc::string_view const framagit_instance_url,
                                        ProjectId const project_id) -> bool {
    constexpr nlc::string_view output_file { "check_is_project_valid.json" };

    nlc::standard_allocator internal_allocator;
    nlc::string_stream command { internal_allocator };
    command << "curl --request GET \"" << framagit_instance_url << "/api/v4/projects/"
            << project_id.value() << "\" --output " << output_file;

    if (execute_command_sync(command) != 0)
        return false;

    auto const json_data = get_json_from_file(internal_allocator, output_file);
    [[maybe_unused]] auto const file_removed { remove_file(output_file) };
    nlc_assert_msg(file_removed, "check_is_project_valid: couldn't delete temp file '", output_file, "'");

    return json_data != nlc::null && is_json_error(*json_data) == false;
}

auto check_is_project_valid(nlc::allocator & allocator,
                            nlc::string_view const framagit_instance_url,
                            ProjectId const project_id) -> AsyncRes<bool> {
    return { nlc::function<bool()> { allocator, [framagit_instance_url, project_id] {
                                        return check_is_project_valid_sync(framagit_instance_url,
                                                                           project_id);
                                    } } };
}

static auto get_succeeded_jobs_sync(nlc::allocator & allocator,
                                    nlc::string_view const framagit_instance_url,
                                    nlc::string_view const private_token,
                                    ProjectId const project_id,
                                    nlc::span<nlc::string const> tag_filters) -> GetSucceededJobsRes {
    constexpr nlc::string_view output_file { "get_succeeded_jobs.json" };

    nlc::string_stream command { allocator };
    command << "curl --request GET --header \"PRIVATE-TOKEN: " << private_token << "\" \""
            << framagit_instance_url << "/api/v4/projects/" << project_id.value()
            << "/jobs?scope[]=success\" --output " << output_file;

    nlc::vector<JobId> res { allocator };

    if (execute_command_sync(command) != 0)
        return CommandExecutionFailed {};

    auto const json_data = get_json_from_file(allocator, output_file);
    [[maybe_unused]] auto const file_removed { remove_file(output_file) };
    nlc_assert_msg(file_removed, "get_succeeded_jobs: couldn't delete temp file '", output_file, "'");
    if (json_data == nlc::null)
        return JsonIsErrorMessage {};

    if (is_json_error(*json_data))
        return JsonIsErrorMessage {};

    if (json_data->is_array() == false)
        return UnexpectedJsonFormat {};

    res.reserve(json_data->size());

    for (auto const & job_desc : *json_data) {
        constexpr nlc::string_view tags_key { "tag_list" };
        constexpr nlc::string_view id_key { "id" };

        if (job_desc.contains(tags_key.ptr()) == false || job_desc.contains(id_key.ptr()) == false)
            return UnexpectedJsonFormat {};

        auto const & tags { job_desc[tags_key.ptr()] };
        auto const match_res { do_tags_match_filters(tags, tag_filters) };
        if (match_res.failed())
            return match_res.forward_error();
        if (match_res.get() == false)
            continue;

        auto const & id_entry { job_desc[id_key.ptr()] };
        if (id_entry.is_number_unsigned() == false || id_entry.is_number_integer() == false)
            return UnexpectedJsonFormat {};

        res.append_no_grow(JobId { id_entry.get<JobId::underlying_type>() });
        nlc::print("job ", id_entry.get<JobId::underlying_type>(), " succeeded");
    }

    return res;
}

auto get_succeeded_jobs(nlc::allocator & allocator,
                        nlc::string_view const framagit_instance_url,
                        nlc::string_view const private_token,
                        ProjectId const project_id,
                        nlc::span<nlc::string const> tag_filters) -> AsyncRes<GetSucceededJobsRes> {
    return { nlc::function<GetSucceededJobsRes()> {
        allocator,
        [&allocator, framagit_instance_url, private_token, project_id, tag_filters] {
            return get_succeeded_jobs_sync(allocator,
                                           framagit_instance_url,
                                           private_token,
                                           project_id,
                                           tag_filters);
        } } };
}

auto dowload_artifacts(nlc::string_view const framagit_instance_url,
                       nlc::string_view const private_token,
                       ProjectId const project_id,
                       JobId const job_id,
                       nlc::string_view const output_path) -> bool {
    nlc::standard_allocator allocator;

    nlc::string_stream command { allocator };
    command << "curl --request GET --header \"PRIVATE-TOKEN: " << private_token << "\" \""
            << framagit_instance_url << "/api/v4/projects/" << project_id.value() << "/jobs/"
            << job_id.value() << "/artifacts\" --output " << output_path;

    return execute_command_sync(command) == 0;
}
