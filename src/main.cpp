#include <stdlib.h>

#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>

#include "settings.hpp"
#include "ui_context.hpp"
#include "window_context.hpp"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#    include <Windows.h>
#    define MAIN() INT WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR, INT)
#else
#    define MAIN() int main()
#endif

static auto init_settings(nlc::allocator & allocator) -> Settings {
    auto load_res { load_settings(allocator) };
    return load_res != nlc::null ? load_res.extract() : Settings { allocator };
}

MAIN() {
    nlc::standard_allocator allocator;

    WindowContext window_ctx { 640, 480, "Framagit version getter" };
    UIContext ui_ctx { window_ctx.get_window() };
    auto settings { init_settings(allocator) };

    while (window_ctx.should_close() == false) {
        window_ctx.begin_frame();
        defer { window_ctx.end_frame(); };

        ui_ctx.update(allocator, settings);
    }

    return EXIT_SUCCESS;
}
