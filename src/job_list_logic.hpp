#pragma once

#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/hash.hpp>

#include "async_res.hpp"
#include "framagit_interface.hpp"
#include "ids.hpp"

struct Settings;
namespace nlc {
class allocator;
}

class JobListLogic final {
  public:
    enum class State {
        invalid_settings,
        fetching_jobs,
        ready,
    };

    auto update(nlc::allocator & allocator, Settings const & settings, bool const are_settings_valid)
        -> void;
    auto get_state() const -> State { return state; }
    auto get_jobs() const -> nlc::span<JobId const>;
    auto get_error_msg() const -> nlc::optional<nlc::string_view> { return error_message; }
    // If the returned id is valid, then we requested its artifacts' download
    auto draw_ui() const -> JobId;

  private:
    nlc::optional<AsyncRes<GetSucceededJobsRes>> fetch_jobs_result;
    State state { State::invalid_settings };
    nlc::optional<nlc::string_view> error_message;
    nlc::hash_result settings_hash { 0u };
};
