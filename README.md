# Framagit artifacts getter

This tiny tool aims to provide a user-friendly way of retrieving artifacts generated by the CI of a Framagit project.
It can be used for any Framagit project and probably for Gitlab projects too.

## Configuration

### Generating a private token

In order for it to work, you will have to generate a private token for your project. This can be done by going to **Settings/Access Tokens** or *&lt;your_project_url&gt;/-/settings/access_tokens*.

![generate_private_token](doc/generate_private_token.png)

Chose whatever name and expiration date you wish. The needed scope is the narrowest, *read_api*, but any choice will work.

Once it's done, click the *Create project access token* and copy the given value.

### Downloading Framagit artifacts getter

Go to the [Releases](https://framagit.org/Gatgat/framagit-artifacts-getter/-/releases) page.
You can download the source code here or the Windows executable.

### Using Framagit artifacts getter

Launch the downloaded executable. Fill in the input values. Your project ID can be found in its landing page.

![find_project_id](doc/find_project_id.png)

Now click on the *"Load succeeded jobs"* button. That will show you the list of the last CI jobs that have succeeded, and give you the possibility to download the artifacts.

## Build it yourself

Framagit artifacts getter is built in top of [nlc](https://framagit.org/perdumondrapeau/nlc), an alternative to the C++ standard library. To build it you will need to use its build system, Meson, and the Conan package manager. Both can be installed via `pip`:
```
pip install meson conan
```

Then you will need to run the following commands
```
git clone git@framagit.org:Gatgat/framagit-artifacts-getter.git
cd framagit-artifacts-getter
meson build
ninja -C build framagit_artifacts_getter  # Add .exe if you're on Windows
# or, if you want to build everything including tests for nlc:
ninja -C build
```

The executable will be stored in `build`.
